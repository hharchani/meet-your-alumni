<?php
	$baseUrl = "http://localhost/alumni/";
	$page_title = array(
		'index' => 'Home',
		'civil-service' => 'Civil Service',
		'management' => 'Management',
		'misc' => 'Miscellaneous',
		'research' => 'Reasearch',
		'startup' => 'Startup'
	);
?>

<!DOCTYPE html>
<html>
<head>
    <title><?php echo $page_title[$current_page];?> | Meet your Alumni</title>
	<meta charset="utf-8" />
	<!-- Include jquery if needed -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>
		(function(w, d, j, s, r, l){if ( ! w[j] ) {var a = d.createElement(s);a[r] = l;var b = d.getElementsByTagName(s)[0];b.parentNode.insertBefore(a,b);}})(window, document, 'jquery', 'script', 'src', 'js/jquery-1.11.2.js');
	</script>
	<!-- -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/custom.css"/>
    <link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="header cf">
        <div class="header-left">
            <a href="http://iiit.ac.in" target="_blank">
                <img id="logo-img" src="images/iiit-logo.png" alt="IIIT Logo" />
            </a>
        </div>
        <div class="header-right">
            <h1 id="page-title"><a class="home-link" href="<?php echo $baseUrl; ?>">Meet your Alumni!</a></h1>
        </div>
    </div>
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo $baseUrl;?>" >Home</a>
			</li>
			<?php
				if ($current_page !== 'index') {
			?>
			<li>
				<a href="#"><?php echo $page_title[$current_page]; ?> ▼ </a>
				<ul class="sub-breadcrumb">
					<?php
						foreach($page_title as $page => $title) {
							if ($page != 'index' && $page != $current_page) {
								echo '<li>';
								echo '<a href="'.$page.'.php">';
								echo $title;
								echo '</a>';
								echo '</li>';
							}
						}
					?>
				</ul>
			</li>
			<?php
				}
			?>
		</ol>
	</div>