<?php
	$current_page = 'index';
	include('common_header.php');
?>
<div class="container-fluid wrapper">
	<p class="description">
		Series of interaction sessions with our Alumni,
		who have done extremely well in their fields,
		admitted in to prestigious schools
		or have choosen unconventional paths.
		Alumni stories inspire us and increase our exposure.
		They can relate themselves to these stories.
		The interactions sessions can be offline (on campus) or online (Video conference).
		We also believe that this is the first step in enhancing Alumni students relations.
		This program will be organized in Association with Alumni Cell.
	</p>
	<div class="row">
		<div class="col-sm-3">
			<a class="tile-link" href="<?php echo $baseUrl; ?>civil-service.php">
				<div class="tile" id="tile1">
					<div class="preview-image-container">
						<img class="preview-image stay-left" src="images/civil-service.jpg" alt="Civil Service Image"/>
					</div>
					<h2>Civil Service</h2>
					<p class="text-justify">
						Some description about this catagory can be here.
					</p>
				</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a class="tile-link" href="<?php echo $baseUrl; ?>research.php">
				<div class="tile" id="tile2">
					<div class="preview-image-container">
						<img class="preview-image" src="images/research.jpg" alt="Research image"/>
					</div>
					<h2>Research</h2>
				</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a class="tile-link" href="<?php echo $baseUrl; ?>management.php">
				<div class="tile" id="tile3">
					<div class="preview-image-container">
						<img class="preview-image stay-left" src="images/management.jpg" alt="Management image"/>
					</div>
					<h2>Management</h2>
				</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a class="tile-link" href="<?php echo $baseUrl; ?>startup.php">
				<div class="tile" id="tile4">
					<div class="preview-image-container">
						<img class="preview-image" src="images/startup.jpg" alt="Startup image"/>
					</div>
					<h2>Startup</h2>
				</div>
			</a>
		</div>
		<div class="col-sm-3">
			<a class="tile-link" href="<?php echo $baseUrl; ?>misc.php">
				<div class="tile" id="tile5">
					<div class="preview-image-container">
						<img class="preview-image" src="images/miscellaneous.png" alt="Miscellaneous image"/>
					</div>
					<h2>Miscellaneous</h2>
				</div>
			</a>
		</div>
	</div>
</div>
<script>
	$(function(){
		var maxHeight = 0;
		$('.preview-image-container').each(function(){
			if ($(this).height() > maxHeight) {
				maxHeight = $(this).height();
			}
		}).height(maxHeight+10);
		$('.tile').each(function(){
			$(this).css('height', $(this).height());
		});
	});
</script>
<?php
	include('common_footer.php');
?>